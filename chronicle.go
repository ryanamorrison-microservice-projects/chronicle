//chronicle library
//a chronicle is a game in WoD terms
package chronicle

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"unicode/utf8"
)

//interface for a chronicle record (as in database record)
type ChronicleRecord interface {
	UpdateName(chronName string)
	Name() string
	ID() string
	Language() string
	OwnerID() string
}

//chronicle data structure
type ChronicleData struct {
	id          string
	name        string
	defaultLang string
	ownerId     string
	//homePage    string
	//staffEmail  string
}

//func UpdateName allows a name of a chronicle to be changed
func (cd *ChronicleData) UpdateName(chronName string) {
	cd.name = chronName
}

//func Name returns the name of a the chronicle
func (cd *ChronicleData) Name() string {
	return cd.name
}

//func ID returns the UUID generated when the chronicle was created
func (cd *ChronicleData) ID() string {
	return cd.id
}

//func Language returns the language setting for the chronicle
func (cd *ChronicleData) Language() string {
	return cd.defaultLang
}

//func OwnerID returns the UUID of the person in the system who created the chronicle
func (cd *ChronicleData) OwnerID() string {
	return cd.ownerId
}

//func NewChronicleData is factory function to create the data for a new chronicle
func NewChronicleData(chronName, language, ownerId string) (*ChronicleData, error) {
	//2 seems small but ideographic languages require less characters
	if utf8.RuneCountInString(chronName) < 3 {
		return &ChronicleData{}, fmt.Errorf("Error: chronicle name must be at least two characters or more")
	}
	newid := uuid.New()
	return &ChronicleData{
		id:          newid.String(),
		name:        chronName,
		defaultLang: language, //TODO: validate this against known values
		ownerId:     ownerId,  //TODO: validate this against owners
		//homePage: ""
		//staffEmail: ""
	}, nil
}

//a Chronicle struct contains a ChronicleRecord
type Chronicle struct {
	cr ChronicleRecord //interface
}

//func ToJSON converts data to JSON
func (c *Chronicle) ToJSON() ([]byte, error) {
	//private fields are not visible to json package
	//use anon struct instead to format the output
	j, err := json.Marshal(struct {
		ID          string `json:"id"`
		Name        string `json:"name"`
		DefaultLang string `json:"default_language"`
		OwnerID     string `json:"owner_id"`
	}{
		ID:          c.cr.ID(),
		Name:        c.cr.Name(),
		DefaultLang: c.cr.Language(),
		OwnerID:     c.cr.OwnerID(),
	})
	if err != nil {
		return nil, err
	}
	return j, nil
}

//func NewChronicle is a factory function to create a new chronicle
func NewChronicle(cr ChronicleRecord) Chronicle {
	return Chronicle{
		cr: cr,
	}
}
