chronicle Library package
=========

An example library written in golang.  See [chronicle-microservice](https://gitlab.com/ryanamorrison-microservice-projects/chronicle-ms) for more information.

Notes
------------

A "chronicle" is the word for a LARP game in White Wolf's World of Darkness parlance.

Dependencies
------------
None at the moment, more to follow as more services are added.  Some of these are being worked on in the `modeling` folder.

License
-------

BSD

Author Information
------------------

Ryan A. Morrison (ryan@ryanamorrison.com)
