package chronicle

import (
	"fmt"
	"github.com/google/uuid"
	"testing"
)

type testData struct {
	Name        string
	Lang        string
	UpdatedName string
	OwnerId     string
}

var want = testData{
	Name:        "Area by night Game",
	Lang:        "eng",
	UpdatedName: "Area by Night Game",
	OwnerId:     "f7c6a14b-c4b3-4f8c-bfb7-d3f78f0ce193",
}

var bad = testData{
	Name:    "AA",
	Lang:    "zzzz",
	OwnerId: "5cf7ef1e-4ce7-4614-8420-dfa26a9be819",
}

func TestChronicleDataCreate(t *testing.T) {
	cd, err := NewChronicleData(want.Name, want.Lang, want.OwnerId)
	if err != nil {
		t.Error(err)
	}
	if _, err := uuid.Parse(cd.ID()); err != nil {
		t.Error(err)
	}
	if got := cd.Name(); want.Name != got {
		t.Errorf("Testing Error, want %s, but got %s\n", want.Name, got)
	}
	if got := cd.Language(); want.Lang != got {
		t.Errorf("Testing Error, want %s, but got %s\n", want.Lang, got)
	}
	if got := cd.OwnerID(); want.OwnerId != got {
		t.Errorf("Testing Error, want %s, but got %s\n", want.OwnerId, got)
	}
	fmt.Printf("%#v\n", cd)
}

func TestChronicleDataUpdate(t *testing.T) {
	cd, err := NewChronicleData(want.Name, want.Lang, want.OwnerId)
	if err != nil {
		t.Error(err)
	}
	cd.UpdateName(want.UpdatedName)
	if got := cd.Name(); want.UpdatedName != got {
		t.Errorf("Testing Error, want %s, but got %s\n", want.UpdatedName, got)
	}
	fmt.Printf("%#v\n", cd)
}

func TestChronicleJSONOutput(t *testing.T) {
	//test output
	cd, err := NewChronicleData(want.Name, want.Lang, want.OwnerId)
	if err != nil {
		t.Error(err)
	}
	c := NewChronicle(cd)
	b, err := c.ToJSON()
	if err != nil {
		t.Error(err)
	}
	fmt.Printf("%s\n", string(b))
}

func TestChronicleErrorHandling(t *testing.T) {
	_, err := NewChronicleData(bad.Name, bad.Lang, bad.OwnerId)
	if err == nil {
		t.Errorf("Expecting error message for creating a chronicle with a name too small, but got nil\n")
	} else {
		fmt.Printf("Bad chronicle name test passed.")
	}
	//add language validation here
}
